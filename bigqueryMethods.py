#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Modulo que obtiene los elementos de un viaje y enviarlos a bigquery"""
activate_this = 'bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

import json
import csv
import pandas as pd
from google.cloud import bigquery
from google.cloud.bigquery import SchemaField

class BigQueryMethods(object):
    """Clase que maneja los metodos para la info en big query"""

    def __init__(self, service_account):
        self.service_account = service_account

    def generar_payload_file(self, schema, data):
        try:
            columns=[]

            for row in data:
                for key in schema:
                    columns.append(key.name)
                break

            filename = "payload.csv"
            df = pd.DataFrame(data)
            df.to_csv(
                filename
                , index=False
                , encoding='utf-8'
                , quoting=csv.QUOTE_NONNUMERIC
                , columns=columns
            )

            return filename
        
        except Exception, ex:
            print(ex)
            return ''

    def cargar_datos(self, dataset_name, table_name, data):
        try:

            # Instantiates a client
            bigquery_client = bigquery.Client.from_service_account_json(self.service_account)
            dataset = bigquery_client.dataset(dataset_name)
            table = dataset.table(table_name)

            # Reload the table to get the schema.
            table.reload()

            # genero archivo con base en schema de tabla
            filename = self.generar_payload_file(table.schema, data)
            if filename == '':
                raise Exception("no fue posible generar el buffer")

            # corro proceso de integracion
            with open(filename, 'rb') as source_file:
                job = table.upload_from_file(
                    source_file, source_format='text/csv', skip_leading_rows=1)

            job.result()  # Wait for job to complete

            print('Loaded {} rows into {}:{}.'.format(
                job.output_rows, dataset_name, table_name))

            return True
            
        except Exception, ex:
            print(ex)
            return False
