import pandas as pd
import csv

data = [
   {
      "sClave":"010000230100",
      "iStockDPN":15,
      "icreated_by":10,
      "dcreated_date":"2017-08-17 10:54:38.726666",
      "iupdate_by":10,
      "sDestino":"473",
      "dupdate_date":"2017-08-17 10:54:38.726666",
      "dFechaDPN":"2017-08-01"
   },
   {
      "sClave":"010000230401",
      "iStockDPN":8,
      "icreated_by":10,
      "dcreated_date":"2017-08-17 10:54:38.726666",
      "iupdate_by":10,
      "sDestino":"473",
      "dupdate_date":"2017-08-17 10:54:38.726666",
      "dFechaDPN":"2017-08-01"
   },
   {
      "sClave":"010000230700",
      "iStockDPN":13,
      "icreated_by":10,
      "dcreated_date":"2017-08-17 10:54:38.726666",
      "iupdate_by":10,
      "sDestino":"473",
      "dupdate_date":"2017-08-17 10:54:38.726666",
      "dFechaDPN":"2017-08-01"
   },
   {
      "sClave":"010000233100",
      "iStockDPN":9,
      "icreated_by":10,
      "dcreated_date":"2017-08-17 10:54:38.726666",
      "iupdate_by":10,
      "sDestino":"473",
      "dupdate_date":"2017-08-17 10:54:38.726666",
      "dFechaDPN":"2017-08-01"
   },
   {
      "sClave":"010000243100",
      "iStockDPN":11,
      "icreated_by":10,
      "dcreated_date":"2017-08-17 10:54:38.726666",
      "iupdate_by":10,
      "sDestino":"473",
      "dupdate_date":"2017-08-17 10:54:38.726666",
      "dFechaDPN":"2017-08-01"
   },
   {
      "sClave":"010000243300",
      "iStockDPN":17,
      "icreated_by":10,
      "dcreated_date":"2017-08-17 10:54:38.726666",
      "iupdate_by":10,
      "sDestino":"473",
      "dupdate_date":"2017-08-17 10:54:38.726666",
      "dFechaDPN":"2017-08-01"
   },
   {
      "sClave":"010000246200",
      "iStockDPN":8,
      "icreated_by":10,
      "dcreated_date":"2017-08-17 10:54:38.726666",
      "iupdate_by":10,
      "sDestino":"473",
      "dupdate_date":"2017-08-17 10:54:38.726666",
      "dFechaDPN":"2017-08-01"
   },
   {
      "sClave":"010000246300",
      "iStockDPN":14,
      "icreated_by":10,
      "dcreated_date":"2017-08-17 10:54:38.726666",
      "iupdate_by":10,
      "sDestino":"473",
      "dupdate_date":"2017-08-17 10:54:38.726666",
      "dFechaDPN":"2017-08-01"
   },
   {
      "sClave":"010000247100",
      "iStockDPN":25,
      "icreated_by":10,
      "dcreated_date":"2017-08-17 10:54:38.726666",
      "iupdate_by":10,
      "sDestino":"473",
      "dupdate_date":"2017-08-17 10:54:38.726666",
      "dFechaDPN":"2017-08-01"
   }
]
df = pd.DataFrame(data)
filename = 'people.csv'
df.to_csv(filename, index=False, encoding='utf-8', quoting=csv.QUOTE_NONNUMERIC)