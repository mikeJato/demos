import pyodbc
import json
import collections
from bigqueryMethods import BigQueryMethods 

conn = pyodbc.connect('DSN=MYSERVER;;UID=Marlen;PWD=M4rlenCh;DATABASE=SILODISA_UMU')
cursor = conn.cursor()

cursor.execute('USP_SELECT_DPN_VISIBILITY \'20170814\'')
	
objects_list = []	
i = 0
for row in cursor:

	i += 1
	if i % 1000 == 0:
		print('leyendo registros desde mssql: {}'.format(i))

	if i == 1001:
		break

	d = {}
	d['sDestino'] = row.sDestino
	d['sClave'] = row.sClave
	d['iStockDPN'] = int(row.iStockDPN)
	#d['dcreated_date'] = str(row.dcreated_date)
	#d['dupdate_date'] = str(row.dupdate_date)
	d['icreated_by'] = int(row.icreated_by)
	d['iupdate_by'] = int(row.icreated_by)
	#d['dFechaDPN'] = str(row.dFechaDPN)
	objects_list.append(d)

conn.close()

bqm = BigQueryMethods('bigquery.json')
bqm.cargar_datos('dev', 'prueba_buena', objects_list)